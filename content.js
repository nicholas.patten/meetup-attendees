/* content.js
Runs in the page itself
Collects data around members that have attended events and communicates it
back to the background script

Author: Nicholas Patten <nicholaspatten92@gmail.com>
*/

// Listens for a message sent from the backend
// Scrapes all names from the current meetups event attendees page
// Then sends all of the names as a response
chrome.runtime.onMessage.addListener(function (msg, sender, sendResponse) {
    if (msg.text === 'report_back') {
        // JQuery for finding the attendees (Requires JQuery library to be included)
        var attendees = []
        $('.card').find('h4').each(function( index ) {
            attendees.push( $( this ).text() )
        });
        sendResponse(attendees);
    }
});