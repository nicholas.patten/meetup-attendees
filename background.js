/* background.js
Runs in the background
Manages browser events e.g. web navigation, tabs, page/ browser actions

Author: Nicholas Patten <nicholaspatten92@gmail.com>
*/

// Regex checks if a url matches an attendes page for a meetup event
var urlRegex = /^https?:\/\/(?:[^./?#]+\.)?meetup\.com\/.*\/events\/.*\/attendees\//

// Listens for when a website has completely loaded
chrome.webNavigation.onCompleted.addListener(function (tab) {
    checkPage(tab.url, tab.tabId)
});

// Listens for any change to tabs
chrome.tabs.onUpdated.addListener(function (tab) {
    if (tab.url && tab.tabId) {
        checkPage(tab.url, tab.tabId)
    }
});

// Checks if the extention should be enabled on the tab
function checkPage(url, tabId) {
    if (urlRegex.test(url)) {
        chrome.pageAction.show(tabId);
    }
}

// Opens html page displaying all attendees of the event
function displayOutput(attendees) {
    chrome.tabs.create({
        url: chrome.extension.getURL('output.html'),
        'active': false
    }, function (tab) {
        var selfTabId = tab.id;
        chrome.tabs.onUpdated.addListener(function (tabId, changeInfo, tab) {
            if (changeInfo.status == "complete" && tabId == selfTabId) {
                // Send data to the page's script:
                var tabs = chrome.extension.getViews({ type: "tab" });
                tabs[0].loadData(attendees);
            }
        });
    })
}

// Listens for when the user clicks the extention
// Then requests attendees from event
chrome.pageAction.onClicked.addListener(function (tab) {
    if (urlRegex.test(tab.url)) {
        chrome.tabs.sendMessage(tab.id, { text: 'report_back' }, displayOutput);
    }
});
