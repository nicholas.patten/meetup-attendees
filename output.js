/* output.js
Populates the output page with attendees from the event

Author: Nicholas Patten
*/

var alreadyRun = false;
function loadData(attendees){
    if (!alreadyRun) {
        var p = document.getElementById("attendees");

        var id=1;
        attendees.forEach(attendee => {
            // Creates a link that searches for the attendee on Linkedin
            var link = encodeURI("https://www.linkedin.com/search/results/index/?keywords=" +attendee+ "&origin=GLOBAL_SEARCH_HEADER")

            // Adds the attendee to the page with Linkedin hyperlink
            p.innerHTML += "<br>" +id+ ": <a href=\"" +link+ "\" target=\"_blank\">" +attendee+ "</a>";
            id++;
        });

        // Needed because opening eg. DevTools to inpect the page
        // will trigger both the "complete" state and the tabId conditions
        // in background.js:
        alreadyRun = true; 
    }
}
